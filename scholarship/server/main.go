package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())

	db, err := gorm.Open("sqlite3", "./sqlite.db")
	if err != nil {
		e.Logger.Fatal(err)
	}

	db = db.Debug()

	db.AutoMigrate(&AppUser{}, &Scholarship{})

	h := &Handler{db}

	e.Use(middleware.CORS())

	e.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
		var u AppUser

		if err := db.First(&u, "username=? AND password=?", username, password).Error; err != nil {
			return false, err
		}

		c.Set("user", &u)
		return true, nil
	}))

	e.GET("/users", h.GetUsers)
	e.POST("/register", h.RegisterUser)
	e.GET("/scholarships", h.GetScholarships)
	e.GET("/scholarships/:id/apply", h.ApplyScholarships)

	e.Logger.Fatal(e.Start(":1323"))
}

package main

import "github.com/jinzhu/gorm"

type AppUser struct {
	gorm.Model
	Username      string       `json:"username"`
	Password      string       `json:"password"`
	ScholarshipID uint         `gorm:"DEFAULT:0" json:"scholarship_id"`
	Scholarship   *Scholarship `json:"scholarship"`
}

type Scholarship struct {
	gorm.Model
	Name    string `json:"name"`
	Level   string `json:"level"`
	Country string `json:"country"`
}

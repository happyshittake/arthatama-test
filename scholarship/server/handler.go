package main

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

type Handler struct {
	DB *gorm.DB
}

func (h *Handler) GetUsers(c echo.Context) error {
	users := []AppUser{}
	hasApply := c.QueryParam("apply")

	queryObj := h.DB.Preload("Scholarship")

	if hasApply != "" {
		queryObj = queryObj.Where("scholarship_id <> 0")
	}

	queryObj.Find(&users)

	return c.JSON(http.StatusOK, users)
}

type registerReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (h *Handler) RegisterUser(c echo.Context) error {
	reqObj := &registerReq{}

	if err := c.Bind(reqObj); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	u := &AppUser{
		Username: reqObj.Username,
		Password: reqObj.Password,
	}

	if err := h.DB.Create(&u).Error; err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.NoContent(http.StatusCreated)
}

func (h *Handler) GetScholarships(c echo.Context) error {
	level := c.QueryParam("level")
	country := c.QueryParam("country")

	queryObj := h.DB

	if level != "" {
		queryObj = queryObj.Where("level=?", level)
	}

	if country != "" {
		queryObj = queryObj.Where("country=?", country)
	}

	scholarships := []Scholarship{}
	queryObj.Find(&scholarships)

	return c.JSON(http.StatusOK, scholarships)
}

func (h *Handler) ApplyScholarships(c echo.Context) error {
	u := c.Get("user").(*AppUser)
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	u.ScholarshipID = uint(id)

	h.DB.Save(u)

	return c.NoContent(http.StatusOK)
}

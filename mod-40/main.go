package main

import "fmt"

func main() {
	sum := 0
	for i := 0; i < 500; i++ {
		if i%4 == 0 {
			sum += i
		}
	}

	fmt.Print(sum)
}

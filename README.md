### requirements
- golang sdk setup

### applications
- Mod 4
    ###### Start:
    ```bash
      go run mod-40/main.go
- stock check
    ###### Start:
    ```bash
      go run stock-check/main.go
- scholarship
    ###### Start:
     ```bash
      go run scholarship/server/*.go
    

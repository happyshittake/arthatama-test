package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	arg := os.Args[1]

	url := fmt.Sprintf("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=%s&apikey=64K81NW62YF7J9HD", arg)

	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var pretty bytes.Buffer
	err = json.Indent(&pretty, b, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	os.Stdout.Write(pretty.Bytes())
}
